#!/bin/bash

# Create symlinks required by the qemu-iotests
ln -sf /usr/libexec/qemu-kvm /usr/lib64/qemu-kvm/tests-src/tests/qemu-iotests/qemu
ln -sf /usr/bin/qemu-io /usr/lib64/qemu-kvm/tests-src/tests/qemu-iotests/qemu-io
ln -sf /usr/bin/qemu-img /usr/lib64/qemu-kvm/tests-src/tests/qemu-iotests/qemu-img
ln -sf /usr/bin/qemu-nbd /usr/lib64/qemu-kvm/tests-src/tests/qemu-iotests/qemu-nbd

cd /usr/lib64/qemu-kvm/tests-src/tests/qemu-iotests/
# Run STABLE set of qemu-iotests
./check -v -raw 001 002 004 005 008 009 010 011 012 021 \
                025 032 033 048 052 063 077 086 101 106 \
                120 140 143 145 150 159 160 162 170 171 \
                175 184 221 226

./check -v -qcow2 001 002 004 005 008 009 010 011 012 017 \
                  018 019 020 021 024 025 027 028 029 032 \
                  033 034 035 037 038 042 046 047 048 050 \
                  052 053 058 062 063 066 068 069 072 073 \
                  074 086 087 089 090 095 098 102 103 105 \
                  107 108 110 111 120 127 133 134 138 140 \
                  141 143 144 145 150 154 156 158 159 162 \
                  170 177 179 182 184 188 190 195 204 217 \
                  226

./check -v -luks 001 002 004 005 008 009 010 011 012 021 \
                 025 032 033 048 052 140 143 145 162 184 \
                 192

./check -v -nbd 001 002 004 005 008 009 010 011 021 032 \
                033 077 094 119 123 143 145 162 184
